<?php

use App\Controllers\Home;
use App\Controllers\Admin\Aduan;
use App\Controllers\Admin\States;
use CodeIgniter\Router\RouteCollection;
use App\Controllers\Admin\UserController;

/**
 * @var RouteCollection $routes
 */


 $routes->get('state/ajaxtable', [States::class, 'ajaxTable'], ['as' => 'admin.state.ajaxtable']);

// $routes->group('admin', static function ($routes) {

//     $routes->get('users/1/23', 'Home::list/1/30');
//     $routes->post('users/1/23', 'Home::index');
    
//     $routes->post('product/(:segment)/(:segment)', 'Home::list/$1/$2');

// });

$routes->get('/', 'Home::index');

// $routes->get('/', [\App\Controllers\Home::class, 'index']);

// $routes->get('/', [Home::class, 'index']);

// // $routes->get('product/(:segment)/(:segment)', 'Home::list/$1/$2');

// $routes->get('product/(:num)/(:segment)', [[Home::class, 'list'], '$1/$2']);

// $routes->view('about', 'pages/about');

$routes->group('admin', static function ($routes) {

    // $routes->get('state', [States::class, 'index']);
    $routes->get('state/show/(:num)', [States::class, 'show'], ['as' => 'admin.state.show']);

    $routes->get('aduan', [Aduan::class, 'index'],['as'=>'admin.aduan.index']);

    $routes->get('aduan2', [Aduan::class, 'index2'],['as'=>'admin.aduan.index2']);

    $routes->get('aduan/ajaxdata', [Aduan::class, 'ajaxData'],['as'=>'admin.aduan.ajaxdata']);

    $routes->get('aduan/new', [Aduan::class, 'new'],['as'=>'admin.aduan.new']);

    $routes->post('aduan/create', [Aduan::class, 'create'],['as'=>'admin.aduan.create']);

    $routes->get('aduan/edit/(:segment)', [Aduan::class, 'edit'],['as'=>'admin.aduan.edit']);

    $routes->put('aduan/update', [Aduan::class, 'update'],['as'=>'admin.aduan.update']);

    $routes->delete('aduan/delete/(:segment)', [Aduan::class, 'delete'],['as'=>'admin.aduan.delete']);

});

$routes->group('admin',['filter'=>'role:pegawai1,user'], static function ($routes) {

    $routes->get('state', [States::class, 'index']);

    $routes->post('user/create', [UserController::class, 'create'],['as'=>'admin.user.create']);
    $routes->get('user/index', [UserController::class, 'index'],['as'=>'admin.user.index']);
    $routes->get('user/edit/(:segment)', [UserController::class, 'edit'],['as'=>'admin.user.edit']);

    $routes->put('user/update', [UserController::class, 'update'],['as'=>'admin.user.update']);

    $routes->get('user/new', [UserController::class, 'new'],['as'=>'admin.user.new']);
 
});



// service('auth')->routes($routes);
service('auth')->routes($routes, ['except' => ['login']]);

$routes->get('login', '\App\Controllers\Auth\LoginController::loginView');
$routes->post('login', '\App\Controllers\Auth\LoginController::loginAction');

// $routes->group('admin',static function(){});