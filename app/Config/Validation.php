<?php

namespace Config;

use CodeIgniter\Config\BaseConfig;
use CodeIgniter\Validation\StrictRules\CreditCardRules;
use CodeIgniter\Validation\StrictRules\FileRules;
use CodeIgniter\Validation\StrictRules\FormatRules;
use CodeIgniter\Validation\StrictRules\Rules;

class Validation extends BaseConfig
{
    // --------------------------------------------------------------------
    // Setup
    // --------------------------------------------------------------------

    /**
     * Stores the classes that contain the
     * rules that are available.
     *
     * @var list<string>
     */
    public array $ruleSets = [
        Rules::class,
        FormatRules::class,
        FileRules::class,
        CreditCardRules::class,
    ];

    /**
     * Specifies the views that are used to display the
     * errors.
     *
     * @var array<string, string>
     */
    public array $templates = [
        'list'   => 'CodeIgniter\Validation\Views\list',
        'single' => 'CodeIgniter\Validation\Views\single',
    ];




    public array  $add_aduan= [
        'complainant_name'=>[
            'label'=>'Nama',
            'rules'=>'required',
            'errors'=>[
                    'required'=>'Sila Isi {field}'
                     ]
                 ],
        'complainant_email'=>[
            'label'=>'Email',
            'rules'=>'required|valid_email',
            'errors'=>[
                    'required'=>'Sila Isi {field}'
                     ]
                 ],
        'complainant_phone'=>[
            'label'=>'No Phone',
            'rules'=>'required',
            'errors'=>[
                    'required'=>'Sila Isi {field}'
                     ]
                 ]

    ];



    // --------------------------------------------------------------------
    // Rules
    // --------------------------------------------------------------------
}
