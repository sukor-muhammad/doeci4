<?= $this->extend('layouts/layout-default') ?>

<!-- page title -->
<?= $this->section('page_title') ?>
<?=$title?>
<?= $this->endSection() ?>
<!--end page title -->


<!-- content -->
<?= $this->section('content') ?>
<?php
$request=service('request');
?>

<?php
// echo '<pre>';
// print_r(validation_list_errors());
// print_r(validation_errors());
// echo '</pre>';
?>

<?=form_open(url_to('admin.user.create'),['id'=>'aduanNew','class'=>'form form-horizontal'])?>

<?= $this->include('admin/user/_form') ?>
<div class="form-body">
                                    <div class="row">
                        <div class="col-md-4">
                                            <label for="contact-info-horizontal-icon">Password confirm</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group has-icon-left <?=validation_show_error('password_confirm')?'is-invalid':''?>">
                                                <div class="position-relative">
                                                    <input value='<?=old('password_confirm',($dataModel->password_confirm??''))?>' name='password_confirm' type="text" class="form-control" placeholder="Mobile" id="contact-info-horizontal-icon">
                                                    <div class="form-control-icon">
                                                        <i class="bi bi-phone"></i>
                                                    </div>

                                                    <div class='parsley-required'>
                                                    <?=validation_show_error('password_confirm')?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        </div>


<div class="col-12 d-flex justify-content-end">
    <button type="button" id='btnAdd' class="btn btn-primary me-1 mb-1">Tambah</button>
     <button type="reset" class="btn btn-light-secondary me-1 mb-1">Reset</button>
  </div>

</form>

<?= $this->endSection() ?>

<?= $this->section('js') ?>

<script>
    $( "#btnAdd" ).on( "click", function() {

        Swal.fire({
  title: "Are you sure?",
  text: "You won't be able to revert this!",
  icon: "warning",
  showCancelButton: true,
  confirmButtonColor: "#3085d6",
  cancelButtonColor: "#d33",
  confirmButtonText: "Yes"
}).then((result) => {
  if (result.isConfirmed) {

    Swal.fire({
      title: "loading",
      text: "prosess.",
      icon: "info",
      showConfirmButton:false,
      allowOutsideClick:false,
      allowEscapeKey:false

    });

    $('#aduanNew').submit();

  }
});
  

    });


</script>


<?= $this->endSection() ?>