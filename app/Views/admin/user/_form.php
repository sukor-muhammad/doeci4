
<?php
$request=service('request');

?>
<div class="form-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="first-name-horizontal-icon">Name</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group has-icon-left <?=validation_show_error('username')?'is-invalid':''?> ">
                                                <div class="position-relative">
                                                    <input value='<?=old('username',($dataModel->username??''))?>' name='username' type="text" class="form-control" placeholder="Name" id="first-name-horizontal-icon">
                                                    <div class="form-control-icon">
                                                        <i class="bi bi-person"></i>
                                                    </div>

                                                    <div class='parsley-required'>
                                                    <?=validation_show_error('username')?>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="email-horizontal-icon">Email</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group has-icon-left <?=validation_show_error('email')?'is-invalid':''?>">
                                                <div class="position-relative">
                                                    <input value='<?=old('email',($dataModel->email??''))?>'  name='email' type="text" class="form-control" placeholder="Email" id="email-horizontal-icon">
                                                    <div class="form-control-icon">
                                                        <i class="bi bi-envelope"></i>
                                                    </div>
                                                    <div class='parsley-required'>
                                                    <?=validation_show_error('email')?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="contact-info-horizontal-icon">Password</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group has-icon-left <?=validation_show_error('password')?'is-invalid':''?>">
                                                <div class="position-relative">
                                                    <input value='<?=old('password',($dataModel->password??''))?>' name='password' type="text" class="form-control" placeholder="Mobile" id="contact-info-horizontal-icon">
                                                    <div class="form-control-icon">
                                                        <i class="bi bi-phone"></i>
                                                    </div>

                                                    <div class='parsley-required'>
                                                    <?=validation_show_error('password')?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                       
                                       
                                        
                                    </div>
                                </div>