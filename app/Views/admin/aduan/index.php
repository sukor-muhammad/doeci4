<?= $this->extend('layouts/layout-default') ?>

<!-- page title -->
<?= $this->section('page_title') ?>
<?=$title?>
<?= $this->endSection() ?>
<!--end page title -->


<!-- content -->
<?= $this->section('content') ?>
<?php
$request=service('request');
?>

<div class="card-body">
                            <form class="form form-horizontal">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="first-name-horizontal-icon">Name</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group has-icon-left">
                                                <div class="position-relative">
                                                    <input value='<?=$request->getGet('complainant_name')?>' name='complainant_name' type="text" class="form-control" placeholder="Name" id="first-name-horizontal-icon">
                                                    <div class="form-control-icon">
                                                        <i class="bi bi-person"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="email-horizontal-icon">Email</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group has-icon-left">
                                                <div class="position-relative">
                                                    <input  name='complainant_email' type="text" class="form-control" placeholder="Email" id="email-horizontal-icon">
                                                    <div class="form-control-icon">
                                                        <i class="bi bi-envelope"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="contact-info-horizontal-icon">Mobile</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group has-icon-left">
                                                <div class="position-relative">
                                                    <input name='complainant_phone' type="number" class="form-control" placeholder="Mobile" id="contact-info-horizontal-icon">
                                                    <div class="form-control-icon">
                                                        <i class="bi bi-phone"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-4">
                                            <label for="contact-info-horizontal-icon">Status</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group has-icon-left">
                                                <div class="position-relative">

                                                   <select class='form-select' name="complainant_status" id="">
                                                    <option value=""></option>
                                                    <?php
                                                    if($lookupstatus??""):
                                                        foreach($lookupstatus as $codes=> $rows):
                                                    ?>
                                                        <option value="<?=$codes?>"><?=$rows?></option>
                                                    <?php
                                                        endforeach;
                                                    endif;
                                                    ?>

                                                   </select>

                                                </div>
                                            </div>
                                        </div>
                                       
                                       
                                        
                                     
                                        <div class="col-12 d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary me-1 mb-1">Cari</button>
                                            <button type="reset" class="btn btn-light-secondary me-1 mb-1">Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>


<div class='table-responsive'>
                        
 <table class='table'>
    <tr>
        <th>Bil</th>
        <th>Name</th>
        <th>complainant_nationality</th>
        <th>complainant_identity</th>
        <th>complainant_email</th>
        <th>complainant_phone</th>
        <th>complainant_status</th>
        
    </tr>
  <?php
  if($datamodel??''):
   foreach($datamodel as $row):
  ?>

    <tr>
        <td><?=++$count?></td>
        <td><?=$row->complainant_name?></td>
        <td><?=$row->name_nationality?></td>
        <td><?=$row->complainant_identity?></td>
        <td><?=$row->complainant_email?></td>
        <td><?=$row->complainant_phone?></td>
        <td><?=$row->name_status?></td>
    </tr>


 <?php
  endforeach;
endif;
 ?>

 </table>
 </div>
  <?=$show?>
 <?= $pager->links('pgs','bootstrap') ?>

<?= $this->endSection() ?>
<!-- end content -->
