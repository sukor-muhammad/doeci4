<?= $this->extend('layouts/layout-default') ?>

<!-- page title -->
<?= $this->section('page_title') ?>
<?=$title?>
<?= $this->endSection() ?>
<!--end page title -->


<!-- content -->
<?= $this->section('content') ?>
<?php
$request=service('request');
?>


                            <form class="form form-horizontal">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="first-name-horizontal-icon">Name</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group has-icon-left">
                                                <div class="position-relative">
                                                    <input value='<?=$request->getGet('complainant_name')?>' name='complainant_name' type="text" class="form-control" placeholder="Name" id="first-name-horizontal-icon">
                                                    <div class="form-control-icon">
                                                        <i class="bi bi-person"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="email-horizontal-icon">Email</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group has-icon-left">
                                                <div class="position-relative">
                                                    <input  name='complainant_email' type="text" class="form-control" placeholder="Email" id="email-horizontal-icon">
                                                    <div class="form-control-icon">
                                                        <i class="bi bi-envelope"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="contact-info-horizontal-icon">Mobile</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group has-icon-left">
                                                <div class="position-relative">
                                                    <input name='complainant_phone' type="number" class="form-control" placeholder="Mobile" id="contact-info-horizontal-icon">
                                                    <div class="form-control-icon">
                                                        <i class="bi bi-phone"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-4">
                                            <label for="contact-info-horizontal-icon">Status</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group has-icon-left">
                                                <div class="position-relative">

                                                   <select class='form-select' name="complainant_status" id="">
                                                    <option value=""></option>
                                                    <?php
                                                    if($lookupstatus??""):
                                                        foreach($lookupstatus as $codes=> $rows):
                                                    ?>
                                                        <option value="<?=$codes?>"><?=$rows?></option>
                                                    <?php
                                                        endforeach;
                                                    endif;
                                                    ?>

                                                   </select>

                                                </div>
                                            </div>
                                        </div>
                                       
                                       
                                        
                                     
                                        <div class="col-12 d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary me-1 mb-1">Cari</button>
                                            <button type="reset" class="btn btn-light-secondary me-1 mb-1">Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
        

<a class='btn btn-info'  href="<?=url_to('admin.aduan.new')?>">Tambah</a>


<div class='table-responsive'>
                        
 <table id='aduanTable' class='table'>
 <thead>
    <tr>
        <th>Bil</th>
        <th>Name</th>
        <th>complainant_nationality</th>
        <th>complainant_identity</th>
        <th>complainant_email</th>
        <th>complainant_phone</th>
        <th>complainant_status</th>
        <th>Tindakan</th>
    </tr>
 </thead>



 </table>
 </div>

<?= $this->endSection() ?>
<!-- end content -->
<?= $this->section('js') ?>



<script>


$(document).on( "click",'.btndelete', function() {

   var id= $(this).data('id');
   

Swal.fire({
title: "Are you sure?",
text: "You won't be able to revert this!",
icon: "warning",
showCancelButton: true,
confirmButtonColor: "#3085d6",
cancelButtonColor: "#d33",
confirmButtonText: "Yes"
}).then((result) => {
if (result.isConfirmed) {

Swal.fire({
title: "loading",
text: "prosess.",
icon: "info",
showConfirmButton:false,
allowOutsideClick:false,
allowEscapeKey:false

});

$('#aduandelete'+id).submit();

}
});


});











    $(document).ready(function() {

        new DataTable('#aduanTable', {

            dom:  '<B><l><f>t<i><p>',
        
            buttons: [
            {
                extend: 'copy',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'excel',
                title: 'Aduan',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdf',
                 orientation: 'landscape',
                    pageSize: 'LEGAL',
                exportOptions: {
                    columns: ':visible',
                   
                }
            },
            'colvis'
     
        ],



            ajax: {
                url:'<?=url_to('admin.aduan.ajaxdata')?>',
                type:'GET',
                data:function(d){
                    d.searchdata=<?=json_encode($request->getGet())?>
                }
            },
            processing: true,
            serverSide: true,
         
        });


    });
</script>




<?= $this->endSection() ?>