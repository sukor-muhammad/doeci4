<?= $this->extend('layouts/layout-default') ?>

<!-- page title -->
<?= $this->section('page_title') ?>
<?=$title?>
<?= $this->endSection() ?>
<!--end page title -->


<!-- content -->
<?= $this->section('content') ?>
<?php
$request=service('request');
?>

<?php
// echo '<pre>';
// print_r(validation_list_errors());
// print_r(validation_errors());
// echo '</pre>';
?>

<?=form_open(url_to('admin.aduan.create'),['id'=>'aduanNew','class'=>'form form-horizontal'])?>

<?= $this->include('admin/aduan/_form') ?>

<div class="col-12 d-flex justify-content-end">
    <button type="button" id='btnAdd' class="btn btn-primary me-1 mb-1">Tambah</button>
     <button type="reset" class="btn btn-light-secondary me-1 mb-1">Reset</button>
  </div>

</form>

<?= $this->endSection() ?>

<?= $this->section('js') ?>

<script>
    $( "#btnAdd" ).on( "click", function() {

        Swal.fire({
  title: "Are you sure?",
  text: "You won't be able to revert this!",
  icon: "warning",
  showCancelButton: true,
  confirmButtonColor: "#3085d6",
  cancelButtonColor: "#d33",
  confirmButtonText: "Yes"
}).then((result) => {
  if (result.isConfirmed) {

    Swal.fire({
      title: "loading",
      text: "prosess.",
      icon: "info",
      showConfirmButton:false,
      allowOutsideClick:false,
      allowEscapeKey:false

    });

    $('#aduanNew').submit();

  }
});
  

    });


</script>


<?= $this->endSection() ?>