
<?php
$request=service('request');


?>
<div class="form-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="first-name-horizontal-icon">Name</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group has-icon-left <?=validation_show_error('complainant_name')?'is-invalid':''?> ">
                                                <div class="position-relative">
                                                    <input value='<?=old('complainant_name',($dataModel->complainant_name??''))?>' name='complainant_name' type="text" class="form-control" placeholder="Name" id="first-name-horizontal-icon">
                                                    <div class="form-control-icon">
                                                        <i class="bi bi-person"></i>
                                                    </div>

                                                    <div class='parsley-required'>
                                                    <?=validation_show_error('complainant_name')?>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="email-horizontal-icon">Email</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group has-icon-left <?=validation_show_error('complainant_email')?'is-invalid':''?>">
                                                <div class="position-relative">
                                                    <input value='<?=old('complainant_email',($dataModel->complainant_email??''))?>'  name='complainant_email' type="text" class="form-control" placeholder="Email" id="email-horizontal-icon">
                                                    <div class="form-control-icon">
                                                        <i class="bi bi-envelope"></i>
                                                    </div>
                                                    <div class='parsley-required'>
                                                    <?=validation_show_error('complainant_email')?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="contact-info-horizontal-icon">Mobile</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group has-icon-left <?=validation_show_error('complainant_phone')?'is-invalid':''?>">
                                                <div class="position-relative">
                                                    <input value='<?=old('complainant_phone',($dataModel->complainant_phone??''))?>' name='complainant_phone' type="text" class="form-control" placeholder="Mobile" id="contact-info-horizontal-icon">
                                                    <div class="form-control-icon">
                                                        <i class="bi bi-phone"></i>
                                                    </div>

                                                    <div class='parsley-required'>
                                                    <?=validation_show_error('complainant_phone')?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                       
                                       
                                        
                                    </div>
                                </div>