<?= $this->extend('layouts/layout-default') ?>

<!-- page title -->
<?= $this->section('page_title') ?>
<?=$title?>
<?= $this->endSection() ?>
<!--end page title -->


<!-- content -->
<?= $this->section('content') ?>



<table class='table'>
    <tr>
        <th>Bil</th>
        <th>Negeri</th>
        <th>kod</th>
         <th>tindakan</th>
    </tr>

    <?php
    if($states??''):
        $bil=0;
        foreach($states as $row):

    ?>
    <tr>
        <td><?=++$bil?></td>
        <td><?=$row['name']?></td>
        <td><?=$row['states_code']?></td>
        <td><a class='btn btn-info' href="<?= url_to('admin.state.show', $row['id']) ?>">paparan</a></td>
    </tr>
            
    <?php
        endforeach;
    endif
    ?>

</table>

    
<table id="example" class="table table-striped" style="width:100%">
    <thead>
        <tr>
            <th>Column 1</th>
            <th>Column 2</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Row 1 Data 1</td>
            <td>Row 1 Data 2</td>
        </tr>
        <tr>
            <td>Row 2 Data 1</td>
            <td>Row 2 Data 2</td>
        </tr>
    </tbody>
</table>


<?= $this->endSection() ?>
<!-- end content -->
<?= $this->section('js') ?>
<script>
        $(document).ready(function () {
    new DataTable('#example', {
    ajax: '<?=url_to('admin.state.ajaxtable')?>',
    processing: true,
    serverSide: true
});
});
    </script>

<?= $this->endSection() ?>