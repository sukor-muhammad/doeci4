<?php

namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

class RoleFilter implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
        $auth = service('auth');

        // Check if the user is logged in
        if (!$auth->loggedIn()) {
            return redirect()->to('/login');
        }

        // Get the user
        $user = $auth->user();

        // Check if the user has at least one of the required roles
        if (!is_array($arguments) || empty($arguments)) {
            return redirect()->to('/unauthorized'); // or another appropriate action
        }

        foreach ($arguments as $role) {
            if ($user->inGroup($role)) {
                return; // User has one of the required roles, allow access
            }
        }

        // User does not have any of the required roles
        return redirect()->to('/unauthorized'); // or another appropriate action
    }

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // Do something here if needed
    }
}
