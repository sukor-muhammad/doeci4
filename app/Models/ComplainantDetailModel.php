<?php

namespace App\Models;

use CodeIgniter\Model;

class ComplainantDetailModel extends Model
{
    protected $table            = 'complainant_details';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = true;
    protected $protectFields    = true;
    protected $allowedFields    = [
                                    'complainant_name',
                                    'complainant_email',
                                    'complainant_phone',
                                    'complainant_status',
                                    'complainant_nationality'
                                ];

    protected bool $allowEmptyInserts = false;
    protected bool $updateOnlyChanged = true;

    protected array $casts = [];
    protected array $castHandlers = [];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    function search($datainput=false){


        if($datainput['complainant_status']??''){

            $this->where('complainant_status',$datainput['complainant_status']);

        }


        if($datainput['complainant_email']??''){

                    $this->like('complainant_email',$datainput['complainant_email']);

        }


         if($datainput['complainant_name']??''){

                    $this->like('complainant_name',$datainput['complainant_name']);

                }


        return $this;




    }



}
