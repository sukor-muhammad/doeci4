<?php

namespace App\Controllers\Admin;

use App\Models\StateModel;
use App\Controllers\BaseController;
use App\Models\ComplainantDetailModel;
use CodeIgniter\HTTP\ResponseInterface;


class States extends BaseController
{

    public function ajaxTable(){


         $draw=   $this->request->getVar('draw');
         $length= (int)  $this->request->getVar('length');
         $start=  (int) $this->request->getVar('start');
         $search=   $this->request->getVar('search');
        $order=    $this->request->getVar('order');

            $model =new ComplainantDetailModel();

            $recordsTotal=$model->countAllResults(false);

            $model->limit($length,$start);

            $recordFilter=$model->countAllResults(false);

           $datamodel= $model->asObject()->get();



           $bil=0;
            if($datamodel->getResult()??''){
                foreach($datamodel->getResult() as $row){


                    $dt['data'][$bil][]= ++$start;
                    $dt['data'][$bil][]=$row->complainant_name;
                    $bil++;


                }

            }

            $dt['draw']=$draw;
            $dt['recordsTotal']=$recordsTotal;
            $dt['recordsFiltered']=$recordFilter;


            return json_encode($dt);
    }
    /**
     * Return an array of resource objects, themselves in array format.
     *
     * @return ResponseInterface
     */
    public function index()
    {

    
        $data['title']='pengurusan negeri';

        $db = \Config\Database::connect();
        // $db = db_connect();



        // $query = $db->query('SELECT *
        // FROM simplecomplain.states');

        // $p=$query->getResult();



        // $builder = $db->table('states');
        // $query   = $builder->get();  
        // $states=$query->getResultArray();

        // $data['states']= $states;

        $model= new StateModel();
        $data['states']=$model->findAll(2,5);

        // dd($data['states']);

     

        return view('admin/states/index',$data);
        // return view('layouts/layout-default',$data);
        
        //
    }

    /**
     * Return the properties of a resource object.
     *
     * @param int|string|null $id
     *
     * @return ResponseInterface
     */
    public function show($id = null)
    {
        $db = \Config\Database::connect();
        $builder = $db->table('states');
        $builder->where(['id'=>$id]);
        $query   = $builder->get();  
        $states=$query->getRow();

        $data['states']= $states;

        dd( $data);


        //
    }

    /**
     * Return a new resource object, with default properties.
     *
     * @return ResponseInterface
     */
    public function new()
    {
        //
    }

    /**
     * Create a new resource object, from "posted" parameters.
     *
     * @return ResponseInterface
     */
    public function create()
    {
        //
    }

    /**
     * Return the editable properties of a resource object.
     *
     * @param int|string|null $id
     *
     * @return ResponseInterface
     */
    public function edit($id = null)
    {
        //
    }

    /**
     * Add or update a model resource, from "posted" properties.
     *
     * @param int|string|null $id
     *
     * @return ResponseInterface
     */
    public function update($id = null)
    {
        //
    }

    /**
     * Delete the designated resource object from the model.
     *
     * @param int|string|null $id
     *
     * @return ResponseInterface
     */
    public function delete($id = null)
    {
        //
    }
}
