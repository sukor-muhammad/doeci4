<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Validation\ValidationRules;
use CodeIgniter\Shield\Entities\User;
use CodeIgniter\HTTP\ResponseInterface;
// use CodeIgniter\RESTful\ResourceController;

class UserController extends BaseController
{
    /**
     * Return an array of resource objects, themselves in array format.
     *
     * @return ResponseInterface
     */
    public function index()
    {

        $datainput= $this->request->getGet();
  

        $model= auth()->getProvider();
    
       

        $datamodel=   $model->asObject()->paginate(10,'pgs');
        $data['datamodel']=$datamodel;
        $data['pager']=$model->pager;

        $datapager=countFrom($model->pager);

        $data['count']=$datapager['count'];
        $data['show']=$datapager['show'];

        

    //   $debug= $model->builder()->db()->getLastQuery();


        // dd($datamodel);
    //     d($debug);
        $data['title']='pengguna';


 return view('admin/user/index',$data);

        //
    }

    /**
     * Return the properties of a resource object.
     *
     * @param int|string|null $id
     *
     * @return ResponseInterface
     */
    public function show($id = null)
    {
        //
    }

    /**
     * Return a new resource object, with default properties.
     *
     * @return ResponseInterface
     */
    public function new()
    {
        //
        $data['title']='add';
        return view('admin/user/new',$data);

    }

    /**
     * Create a new resource object, from "posted" parameters.
     *
     * @return ResponseInterface
     */
    public function create()
    {

        $users = auth()->getProvider();




        // $datain=[
        //     'username' => 'alikacak',
        //     'email'    => 'tetst@t.com',
        //     'password' => '1qaz@wsx',
        //     'password_confirm'=>'1qaz@wsx',
        // ];




        $rules=new ValidationRules();

        $rulesReg=$rules->getRegistrationRules();

        $datain = $this->request->getPost(array_keys($rulesReg));


        if(!$this->validateData($datain,$rulesReg)){

            // dd($this->validator->getErrors());
            return redirect()->back()->withInput();

        }



        $user = new User($datain);
        

        if($users->save($user)){


             // To get the complete user object with ID, we need to get from the database
        $user = $users->findById($users->getInsertID());

        // Add to default group
        // $users->addToDefaultGroup($user);
        $groupuser='pegawai1';
        $user->addGroup($groupuser);


        return redirect()->route('admin.user.index');


        //

        }

       


    }

    /**
     * Return the editable properties of a resource object.
     *
     * @param int|string|null $id
     *
     * @return ResponseInterface
     */
    public function edit($id = null)
    {
        // Get the User Provider (UserModel by default)
        $users = auth()->getProvider();

        // Find by the user_id
        $user = $users->findById($id);

        $data['title']='kemaskini';
        $data['dataModel']=$user;

       

        return view('admin/user/edit',$data);
    }

    /**
     * Add or update a model resource, from "posted" properties.
     *
     * @param int|string|null $id
     *
     * @return ResponseInterface
     */
    public function update($id = null)
    {

        $id=$this->request->getPost('id');

        $users = auth()->getProvider();

        $user = $users->findById($id);

        
        $user->fill([
            'username' => $this->request->getPost('username'),
            'email' =>  $this->request->getPost('email'),
            'password' => $this->request->getPost('password'),
        ]);


      

        if($users->save($user)){

            return redirect()->route('admin.user.index');
        }
        //
    }

    /**
     * Delete the designated resource object from the model.
     *
     * @param int|string|null $id
     *
     * @return ResponseInterface
     */
    public function delete($id = null)
    {
        $users = auth()->getProvider();

        $users->delete($id, true);
        //
    }
}
