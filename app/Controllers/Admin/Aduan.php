<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\ComplainantDetailModel;
use CodeIgniter\HTTP\ResponseInterface;


class Aduan extends BaseController
{
    /**
     * Return an array of resource objects, themselves in array format.
     *
     * @return ResponseInterface
     */
    public function index()
    {



       $datainput= $this->request->getGet();
        // $this->request->getVar();
        // $this->request->getPost();
        // $this->request->getPostGet();


        $model= new ComplainantDetailModel();
        // $model->from([],true);
        // $model->from('complainant_details as cd');
        $model->select('complainant_details.*,
                        al.code as code_nationality,
                        al.name as name_nationality,
                        al2.code as code_status,
                        al2.name as name_status,
                        s.name as state_name
                        ');
        $model->join('asset_lookup as al', 'al.code = complainant_details.complainant_nationality','left');
        $model->join('asset_lookup al2', 'al2.code = complainant_details.complainant_status','left');
        $model->join('states s', 's.id = complainant_details.state_id','left');

        $model->search($datainput);



        // if($datainput['complainant_status']??''){

        //     $model->where('complainant_status',$datainput['complainant_status']);

        // }


        // if($datainput['complainant_email']??''){

        //             $model->like('complainant_email',$datainput['complainant_email']);

        //         }

        
        // 1
        // if($this->request->getGet('complainant_name')??''){

        //     $model->like('complainant_name',$this->request->getGet('complainant_name'));

        // }
        // end 1

        // //2
        // if($datainput['complainant_name']??''){

        //     $model->like('complainant_name',$datainput['complainant_name']);

        // }
        // //end2

        // //3
        // $complainant_name=$this->request->getGet('complainant_name');

        // if($complainant_name??''){

        //     $model->like('complainant_name',$complainant_name);

        // }
        //end 3



        // $model->where(['state_id'=>5,'town_id'=>13]);

        // $model->whereIn('department_id', static function ($builder) {
        //     $builder->select('id')->from('department')->where('department_category', 'Administration');
        // });




        $datamodel=   $model->asObject()->paginate(10,'pgs');
        $data['datamodel']=$datamodel;
        $data['pager']=$model->pager;

        $datapager=countFrom($model->pager);

        $data['count']=$datapager['count'];
        $data['show']=$datapager['show'];

        

    //   $debug= $model->builder()->db()->getLastQuery();


        // dd($datamodel);
    //     d($debug);
        $data['title']='aduan';
        $data['lookupstatus']=getStatusComp();

 return view('admin/aduan/index',$data);
        //
    }


    function index2(){

        $data['title']='datatable';
        $data['lookupstatus']=getStatusComp();

        return view('admin/aduan/index2',$data);
    }


    function ajaxData(){

        // echo '<pre>';
        // print_r($this->request->getVar());
        // echo '</pre>';
        // die();

        $column[1]='complainant_details.complainant_name';
        $column[2]='al.name';
        $column[3]='complainant_details.complainant_identity';
        $column[4]='complainant_details.complainant_email';
        $column[5]='complainant_details.complainant_phone';
        $column[6]='al2.name';



       $draw= $this->request->getGet('draw');
       $start= (int) $this->request->getGet('start');
       $length= (int) $this->request->getGet('length');
       $datainput=  $this->request->getGet('searchdata');
       $search=  $this->request->getGet('search');
       $order=  $this->request->getGet('order');



        $model= new ComplainantDetailModel();
        $model->select('complainant_details.*,
        al.code as code_nationality,
        al.name as name_nationality,
        al2.code as code_status,
        al2.name as name_status,
        s.name as state_name
        ');
$model->join('asset_lookup as al', 'al.code = complainant_details.complainant_nationality','left');
$model->join('asset_lookup al2', 'al2.code = complainant_details.complainant_status','left');
$model->join('states s', 's.id = complainant_details.state_id','left');

$recordTotal=  $model->countAllResults(false);

  $model->search($datainput);  

// serch 
  if($search['value']??''){

    $model->groupStart();

    foreach($column as $rowsrch){

        $model->orLike($rowsrch,$search['value'],'both');

    }

    $model->groupEnd();
  }
// end serch

// orderby
if($order[0]['column']??''){
    $nocolumn=$order[0]['column'];
    $nocolumnorder=$column[$nocolumn];
    $orderby=$order[0]['dir'];


 $model->orderBy($nocolumnorder,$orderby);


}





// orderby


$recordsFiltered=  $model->countAllResults(false);


$model->limit($length,$start);

$dataModel=$model->get();

//   $debug= $model->builder()->db()->getLastQuery();


//         dd($debug);

      $bil=0;
        if($dataModel->getNumRows()>0):
            foreach ($dataModel->getResult() as $row):
                $id=$row->id;
               $eid= encode_custom($row->id);

               $deleteurl=url_to('admin.aduan.delete',$eid);

                

                $delete='<form id="aduandelete'.$id.'" action="'.$deleteurl.'" method="POST">
                <input type="hidden" name="_method" value="DELETE">
                <input type="hidden" name="'.csrf_token().'" value="'.csrf_hash().'">
                <button type="button" data-id="'.$id.'" class="btn btn-danger btndelete"  >Padam</button>
               </form>';


                $dt['data'][$bil][]=++$start;
                $dt['data'][$bil][]=$row->complainant_name;
                $dt['data'][$bil][]=$row->name_nationality;
                $dt['data'][$bil][]=$row->complainant_identity;
                $dt['data'][$bil][]=$row->complainant_email;
                $dt['data'][$bil][]=$row->complainant_phone;
                $dt['data'][$bil][]=$row->name_status;
                $dt['data'][$bil][]="<a class='btn btn-default'
                 href='".url_to('admin.aduan.edit',$eid)."'>Kemaskini</a> $delete";
                # code...
                
                $bil++;

            endforeach;

        endif;

        $dt['draw']=$draw;
        $dt['recordsTotal']=$recordTotal;
        $dt['recordsFiltered']=$recordsFiltered;


        return json_encode($dt);






    }



    /**
     * Return the properties of a resource object.
     *
     * @param int|string|null $id
     *
     * @return ResponseInterface
     */
    public function show($id = null)
    {
        //
    }

    /**
     * Return a new resource object, with default properties.
     *
     * @return ResponseInterface
     */
    public function new()
    {
        $data['title']='Tambah';
        // $data['lookupstatus']=getStatusComp();

        return view('admin/aduan/new',$data);
    }

    /**
     * Create a new resource object, from "posted" parameters.
     *
     * @return ResponseInterface
     */
    public function create()
    {

        $datainput=$this->request->getPost();

       

        // $rules=[
        //     'complainant_name'=>[
        //         'label'=>'Nama',
        //         'rules'=>'required',
        //         'errors'=>[
        //                 'required'=>'Sila Isi {field}'
        //                  ]
        //              ],
        //     'complainant_email'=>[
        //         'label'=>'Email',
        //         'rules'=>'required|valid_email',
        //         'errors'=>[
        //                 'required'=>'Sila Isi {field}'
        //                  ]
        //              ],
        //     'complainant_phone'=>[
        //         'label'=>'No Phone',
        //         'rules'=>'required',
        //         'errors'=>[
        //                 'required'=>'Sila Isi {field}'
        //                  ]
        //              ],

        // ];

        $rules=config('Validation')->add_aduan;

  

        $datainput = $this->request->getPost(array_keys($rules));

        

        if(!$this->validateData($datainput,$rules)){

                return redirect()->back()->withInput();

         }


         $model= new ComplainantDetailModel();
         $validatedata=$this->validator->getValidated();

         $validatedata['complainant_status']='STS003';
         $validatedata['complainant_nationality']='CIT001';


         if($model->insert($validatedata)){

            return redirect()->route('admin.aduan.index2');

         }





    //    dd($this->validator->getErrors());

    // $validation = service('validation');

    // $validation->setRules($rules);

    // if(!$validation->run($datainput)) {

    // }

    // dd($validation->getErrors());



        





        


        //
    }

    /**
     * Return the editable properties of a resource object.
     *
     * @param int|string|null $id
     *
     * @return ResponseInterface
     */
    public function edit($id = null)
    {

       $id= decode_custom($id);

      
       $model= new ComplainantDetailModel();

       $dataModel=$model->asObject()->find($id);


       $data['title']='kemaskini';
       $data['dataModel']=$dataModel;

       return view('admin/aduan/edit',$data);


        //
    }

    /**
     * Add or update a model resource, from "posted" properties.
     *
     * @param int|string|null $id
     *
     * @return ResponseInterface
     */
    public function update($id = null)
    {
        
        $rules=config('Validation')->add_aduan;


        $datainput = $this->request->getPost(array_keys($rules));

        

        if(!$this->validateData($datainput,$rules)){

                return redirect()->back()->withInput();

         }


    $validatedata=$this->validator->getValidated();
    $id=$this->request->getPost('id');

    $model= new ComplainantDetailModel();


        if($model->update($id,$validatedata)){


                return redirect()->route('admin.aduan.index2');

         }




        //
    }

    /**
     * Delete the designated resource object from the model.
     *
     * @param int|string|null $id
     *
     * @return ResponseInterface
     */
    public function delete($id = null)
    {
        $id= decode_custom($id);

      
        $model= new ComplainantDetailModel();

        if($model->delete($id)){

            return redirect()->route('admin.aduan.index2');

        }


        //
    }
}
