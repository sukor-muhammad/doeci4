<?php

namespace App\Controllers;

class HomeTest extends BaseController
{
    public function index(): string
    {
        return view('welcome_message');
    }

    public function getTest(): string
    {
        return view('welcome_message');
    }
}
