<?php

namespace App\Validation;

use CodeIgniter\Shield\Validation\ValidationRules as sheildvalidation;

class ValidationRules extends sheildvalidation
{

    public function getRegistrationRules(): array
    {
        $setting = setting('Validation.registration');
        if ($setting !== null) {
            return $setting;
        }

        $usernameRules            = $this->config->usernameValidationRules;
        $usernameRules['rules'][] = sprintf(
            'is_unique[%s.username]',
            $this->tables['users']
        );

        $emailRules            = $this->config->emailValidationRules;
        $emailRules['rules'][] = sprintf(
            'is_unique[%s.secret]',
            $this->tables['identities']
        );

        $passwordRules            = $this->getPasswordRules();
        $passwordRules['rules'][] = 'strong_password[]';

        return [
            'username'         => $usernameRules,
            'email'            => $emailRules,
            'password'         => $passwordRules,
            'password_confirm' => $this->getPasswordConfirmRules(),
        ];
    }

    public function getLoginRules(): array
    {
        return setting('Validation.login') ?? [
            'username' => $this->config->usernameValidationRules,
            // 'email'    => $this->config->emailValidationRules,
            'password' => $this->getPasswordRules(),
        ];
    }

}