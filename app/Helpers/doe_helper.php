<?php

use App\Models\AssetLookupModel;


function encode_custom($id){

    $ecy=\Config\Services::encrypter();

    $eid=$ecy->encrypt($id);
    $eid=bin2hex($eid);

    return $eid;


}

    function decode_custom($id){


    if (ctype_xdigit($id) && strlen($id) % 2 == 0) {
        $encrypter = \Config\Services::encrypter();
        $id = $encrypter->decrypt(hex2bin($id));   
    } else {
        throw new \Exception('error');
    }

    return $id;

    }






function testHelper(){

     return 'test';
}



function getStatusComp(){
    $d=null;
    $model=new AssetLookupModel();
    $model->where(['category'=>'complaint_status']);
   $datastatus= $model->findAll();

   if($datastatus??''){

        foreach($datastatus as $row){

            // dd($row['code']);
            $d[$row['code']]=$row['name'];

        }

   }


   return $d;
    
}

function countFrom($pager){

    $totalAll=$pager->getTotal('pgs');
    
    $getperPage=$pager->getperPage('pgs');
    $currentPage=$pager->getcurrentPage('pgs');
    $pageCount=$pager->getpageCount('pgs');
    
    // if($currentPage > 1 && $currentPage !=$pageCount ){
        if($currentPage > 1  ){
        $currentPagenow=$currentPage-1;
        $total=$getperPage*$currentPagenow;
    
    // }elseif($currentPage ==$pageCount){
    
    //     $total=$totalAll-1;
    
    }else{
        $total=0;
    }
    
    
    
    $showStart=$total;
    $end=$getperPage;
    
    if( $currentPage !=$pageCount){
    
        $end=$total+$getperPage;
    }else{
        $end=$totalAll;
    }
    
    
    if($totalAll==0){
        $showStart=0;
        $end=0;
        $show= "Showing " .$showStart." to $end of $totalAll results";
    }else{
        $show= "Showing " .++$showStart." to $end of $totalAll results";
    }
    
    
    
    $d['show']=$show;
    $d['count']=$total;
    
    return $d;
    
    }